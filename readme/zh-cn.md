# Littlekan I18N

## 简介：

------------------------

一个国际化小插件，让程序国际化不再困难！

## 安装教程：

------------------------

### C++：
#### 使用Littlekan Package Manager(LKPM)：
```
lkpm install lki18n -pt cpp -v $latest
```
#### 直接使用：
[点此](../../../tree/master/cpp/src)进行代码拷贝或[戳此](../../../releases)下载

------------------------

### 网页端：
#### 使用Littlekan Package Manager(LKPM)：
```
lkpm install lki18n -pt web -v $latest
```

#### 直接使用：
[点此](../../../tree/master/web/src)进行代码拷贝或[戳此](../../../releases)下载

------------------------

### 两端都要？
#### 直接——
```
lkpm install lki18n -v $latest
```
#### 或者——
[点此](../../../../releases)下载所有代码

------------------------

Copyright (C) 2021 [littlekan](../../../../../littlekan).

Licensed under the [Apache-2.0](http://choosealicense.com/licenses/apache-2.0/?lkref=project.footer*license)